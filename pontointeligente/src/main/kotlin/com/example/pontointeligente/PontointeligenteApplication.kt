package com.example.pontointeligente

import com.example.pontointeligente.documents.Empresa
import com.example.pontointeligente.documents.Funcionario
import com.example.pontointeligente.enums.PerfilEnum
import com.example.pontointeligente.repositories.EmpresaRepository
import com.example.pontointeligente.repositories.FuncionarioRepository
import com.example.pontointeligente.repositories.LancamentoRepository
import com.example.pontointeligente.utils.SenhaUtils
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration

/**
 * @todo
 * O código abaixo serve somente para testes e deve ser removido ao término
 * do projeto, até porque ele faz alguns testes unitário falharem.
 *
 * Mantenha somente a declaração da classe e o método main, conforme criado no início do curso.
 */

@SpringBootApplication(exclude = arrayOf(SecurityAutoConfiguration::class))
class PontointeligenteApplication(val empresaRepository: EmpresaRepository,
								  val funcionarioRepository: FuncionarioRepository,
								  val lancamentoRepository: LancamentoRepository) : CommandLineRunner {

	override fun run(vararg args: String?) {
		empresaRepository.deleteAll()
		funcionarioRepository.deleteAll()
		lancamentoRepository.deleteAll()

		var empresa: Empresa = Empresa("Empresa", "10443887000146")
		empresa =  empresaRepository.save(empresa)

		var admin: Funcionario = Funcionario(
				"Chico",
				"Chico@gmail.com",
				SenhaUtils().gerarBcrypt("chico10"),
				"3456789000",
				PerfilEnum.ROLE_USUARIO,
				empresa.id!!
		)
		admin = funcionarioRepository.save(admin)

		var funcionario: Funcionario = Funcionario("Funcionario",
				"funcionario@empresa.com", SenhaUtils().gerarBcrypt("123456"),
				"44325441557", PerfilEnum.ROLE_USUARIO, empresaId = empresa.id)
		funcionario = funcionarioRepository.save(funcionario)

		println("Empresa ID: " + empresa.id)
		println("Admin ID: " + admin.id)
		println("Funcionario ID: " + funcionario.id)
	}

}

fun main(args: Array<String>) {
	SpringApplication.run(PontointeligenteApplication::class.java, *args)
}