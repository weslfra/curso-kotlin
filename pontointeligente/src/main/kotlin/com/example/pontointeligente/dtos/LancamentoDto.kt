package com.example.pontointeligente.dtos

import org.springframework.data.annotation.Id
import javax.validation.constraints.NotEmpty

data class LancamentoDto(

        @get: NotEmpty(message = "Data não pode ser vazia.")
        val data: String? = null,

        @get: NotEmpty(message = "Tipo não pode ser vazio.")
        val tipo: String? = null,

        val descricao: String? = null,
        val localizacao: String? = null,
        val funcionarioId: String? = null,
        @Id val id: String? = null
)