package com.example.pontointeligente.utils

//import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Test
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.util.Assert

class SenhaUtilsTest {

    private val SENHA = "chico10"
    private val bCryptEncoder = BCryptPasswordEncoder()

    @Test
    fun testGerarHashSenha(){
        val hash = SenhaUtils().gerarBcrypt(SENHA)
        Assert.isTrue(bCryptEncoder.matches(SENHA,hash))
    }
}