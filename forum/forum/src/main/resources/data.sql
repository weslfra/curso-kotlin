INSERT INTO USUARIO(nome, email, senha) VALUES('Aluno_1', 'aluno_1@email.com', '$2a$10$4satMrTmc/6ngT6jGqnBhuwZcfAeleTY4cf7B3vqVC.CjkglIH/sS');
INSERT INTO USUARIO(nome, email, senha) VALUES('Aluno_2', 'aluno_2@email.com', '222222');
INSERT INTO USUARIO(nome, email, senha) VALUES('Aluno_3', 'aluno_3@email.com', '333333');

INSERT INTO PERFIL(nome) VALUES('Aluno_3');

INSERT INTO CURSO(nome, categoria) VALUES('Spring Boot', 'Programação');
INSERT INTO CURSO(nome, categoria) VALUES('HTML 5', 'Front-end');
INSERT INTO CURSO(nome, categoria) VALUES('Java', 'Programacao,');

INSERT INTO TOPICO(titulo, mensagem, data_criacao, status, autor_id, curso_id) VALUES('Dúvida', 'Erro ao criar projeto', '2019-05-05 18:00:00', 'NAO_RESPONDIDO', 1, 1);
INSERT INTO TOPICO(titulo, mensagem, data_criacao, status, autor_id, curso_id) VALUES('Dúvida 2', 'Projeto não compila', '2019-05-05 19:00:00', 'NAO_RESPONDIDO', 2, 2);
INSERT INTO TOPICO(titulo, mensagem, data_criacao, status, autor_id, curso_id) VALUES('Dúvida 3', 'Tag HTML', '2019-05-05 20:00:00', 'NAO_RESPONDIDO', 3, 3);

INSERT INTO RESPOSTA(data_criacao, mensagem, solucao, topico_id) VALUES('2019-05-05 20:00:00','Tag HTML', false, 2);

