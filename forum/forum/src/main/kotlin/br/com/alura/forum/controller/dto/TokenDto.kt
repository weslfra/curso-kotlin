package br.com.alura.forum.controller.dto

class TokenDto(
        token: String,
        tipo: String
) {
    var token: String = ""
    var tipo: String = ""

    init {
        this.token = token
        this.tipo = tipo
    }

}
