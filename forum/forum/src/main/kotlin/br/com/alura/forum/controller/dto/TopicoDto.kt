package br.com.alura.forum.controller.dto

import br.com.alura.forum.modelo.Topico
import org.springframework.data.domain.Page
import java.time.LocalDateTime

class TopicoDto(topico: Topico) {

    var id: Long
    var titulo: String
    var mensagem: String
    var dataCriacao: LocalDateTime

    init {
        this.id = topico.id
        this.titulo = topico.titulo
        this.mensagem = topico.mensagem
        this.dataCriacao = topico.dataCriacao
    }

    companion object{
        fun converter (topicos: Page<Topico>): Page<TopicoDto>{
            //return topicos.map { TopicoDto(it) }
            return topicos.map { TopicoDto(it) }
        }

        fun converter2 (topicos: List<Topico>): List<TopicoDto>{
                return topicos.map { TopicoDto(it) }
        }
    }

}