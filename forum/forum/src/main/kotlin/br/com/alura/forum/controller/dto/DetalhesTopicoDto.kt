package br.com.alura.forum.controller.dto
import br.com.alura.forum.modelo.StatusTopico
import br.com.alura.forum.modelo.Topico
import java.time.LocalDateTime
import java.util.ArrayList
import java.util.stream.Collectors
import java.util.stream.Collectors.toList


class  DetalhesTopicoDto(topico: Topico) {

    val id: Long
    val titulo: String
    val mensagem: String
    val dataCriacao: LocalDateTime
    val nomeAutor: String
    val status: StatusTopico
    val respostas: ArrayList<RespostaDto>

    init {
        this.id = topico.id
        this.titulo = topico.titulo
        this.mensagem = topico.mensagem
        this.dataCriacao = topico.dataCriacao
        this.nomeAutor = topico.autor.nome!!
        this.status = topico.status
        this.respostas = ArrayList<RespostaDto>()
        this.respostas.addAll(RespostaDto.converter(listOf(topico)))

    }
}

