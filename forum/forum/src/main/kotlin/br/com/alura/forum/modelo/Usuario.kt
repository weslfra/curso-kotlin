package br.com.alura.forum.modelo

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import javax.persistence.*

@Entity
class Usuario(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "autor_id")
        var id: Long? = 0L,
        var nome: String? = "",
        var email: String? = "",
        var senha: String? = ""
) : UserDetails {

    @ManyToMany(fetch = FetchType.EAGER)
    var perfis: List<Perfil> = ArrayList()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Usuario

        if (id != other.id) return false
        if (nome != other.nome) return false
        if (email != other.email) return false
        if (senha != other.senha) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + nome.hashCode()
        result = 31 * result + email.hashCode()
        result = 31 * result + senha.hashCode()
        return result
    }

    override fun getAuthorities(): List<Perfil> {
        return this.perfis
    }

    override fun getPassword(): String? {
        return this.senha
    }

    override fun getUsername(): String? {
        return this.nome
    }

    override fun isAccountNonExpired(): Boolean {
        return true

    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isEnabled(): Boolean {
        return true
    }


}