package br.com.alura.forum.config.security

import br.com.alura.forum.modelo.Usuario
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Service
import java.util.*

@Service

class TokenService() {

    @Value("\${forum.jwt.expiration}")
    lateinit var expiration: String

    @Value("\${forum.jwt.secret}")
    lateinit var secret: String

    fun gerarToken(authentication: Authentication): String {
        val logado: Usuario = authentication.principal as Usuario
        val hoje: Date = Date()
        var dataExpiracao: Date = Date(hoje.time + expiration.toLong())

        return Jwts.builder()
                .setIssuer("API do Fórum da Alura")
                .setSubject(logado.id.toString())
                .setIssuedAt(hoje)
                .setExpiration(dataExpiracao)
                .signWith(SignatureAlgorithm.HS256,secret)
                .compact()
    }

    fun isTokenValido(token: String?): Boolean {
        try {
            Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token)
            return true
        } catch (e: Exception) {
            return false
        }

    }
    fun getIdUsuario(token: String?): Long {
        val claims = Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token).body
        return claims.subject.toLong()
    }
}