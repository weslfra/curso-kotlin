package br.com.alura.forum.modelo

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.security.core.GrantedAuthority;

@Entity
class Perfil(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        private val id: Long,
        private val nome: String
) : GrantedAuthority {

    override fun getAuthority(): String {
        return this.nome
    }
}


