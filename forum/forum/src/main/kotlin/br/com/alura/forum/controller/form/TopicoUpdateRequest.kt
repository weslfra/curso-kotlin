package br.com.alura.forum.controller.form

import br.com.alura.forum.modelo.Topico
import br.com.alura.forum.repository.TopicoRepository
import org.hibernate.validator.constraints.Length
import org.jetbrains.annotations.NotNull
import javax.validation.constraints.NotEmpty

class
TopicoUpdateRequest(

        @field: NotNull
        @field: NotEmpty(message = "Por Favor, informe o Titulo da Mensagem")
        @field: Length(min = 5)
        var titulo: String,

        @field: NotNull
        @field: NotEmpty(message = "Por favor, digite a mensagem")
        @field: Length(min=10)
        val mensagem: String
) {

        fun atualizar(id: Long, topicoRepository: TopicoRepository): Topico {
                val topico: Topico = topicoRepository.getOne(id)
                topico.titulo = this.titulo
                topico.mensagem = this.mensagem
                return topico
        }
}
