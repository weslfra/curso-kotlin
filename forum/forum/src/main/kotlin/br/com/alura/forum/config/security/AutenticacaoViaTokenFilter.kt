package br.com.alura.forum.config.security

import br.com.alura.forum.modelo.Usuario
import br.com.alura.forum.repository.UsuarioRepository
import io.jsonwebtoken.Jwts
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class AutenticacaoViaTokenFilter(
        val tokenService: TokenService,
        val repository: UsuarioRepository
): OncePerRequestFilter() {


    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {

        val token: String? = recuperarToken(request)

        val valido: Boolean = tokenService.isTokenValido(token)
        if (valido) {
            autenticarUsuario(token)
        }

        filterChain.doFilter(request, response)
    }

    private fun autenticarUsuario(token: String?) {
        val idUsuario: Long = tokenService.getIdUsuario(token)
        val usuario: Usuario = repository.findById(idUsuario).get()

        val authentication = UsernamePasswordAuthenticationToken(usuario, null, usuario.authorities)
        SecurityContextHolder.getContext().setAuthentication(authentication)
    }

    private fun recuperarToken(request: HttpServletRequest): String? {
        val token: String = request.getHeader("Authorization")
        if (token == null || token.isEmpty() || !token.startsWith("Bearer ")) {
            return null
        }
        return token.substring(7, token.length)

    }
}



