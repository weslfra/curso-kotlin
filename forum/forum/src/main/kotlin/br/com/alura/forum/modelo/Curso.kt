package br.com.alura.forum.modelo

import javax.persistence.*

@Entity
class Curso(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name= "curso_id")
        var id: Long = 0L,
        var nome: String = "",
        var categoria: String = ""
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Curso

        if (id != other.id) return false
        if (nome != other.nome) return false
        if (categoria != other.categoria) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + nome.hashCode()
        result = 31 * result + categoria.hashCode()
        return result
    }
}