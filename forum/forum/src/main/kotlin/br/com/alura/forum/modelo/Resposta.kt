package br.com.alura.forum.modelo

import java.time.LocalDateTime
import javax.persistence.*

@Entity
class Resposta {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0L
    var mensagem: String = ""

    @ManyToOne
    var topico: Topico = Topico()
    var data_Criacao = LocalDateTime.now()


    @ManyToOne
    var autor: Usuario = Usuario()
    var solucao: Boolean = false
}

