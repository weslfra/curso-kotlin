package br.com.alura.forum.controller

import br.com.alura.forum.controller.dto.DetalhesTopicoDto
import br.com.alura.forum.controller.dto.TopicoDto
import br.com.alura.forum.controller.form.TopicoRequest
import br.com.alura.forum.controller.form.TopicoUpdateRequest
import br.com.alura.forum.modelo.Curso
import br.com.alura.forum.modelo.Topico
import br.com.alura.forum.repository.CursoRepository
import br.com.alura.forum.repository.TopicoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.web.PageableDefault
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI
import java.util.*
import javax.transaction.Transactional
import javax.validation.Valid

@RestController
@RequestMapping("/topicos")
class TopicosController {

    @Autowired
    lateinit var topicoRepository: TopicoRepository

    @Autowired
    lateinit var cursoRepository: CursoRepository

    @GetMapping("/topicoMemoria")
    fun listaMemoria(): List<TopicoDto> {

        val topico = Topico(
                titulo = "Alura ",
                mensagem = "Meu primeiro Curso",
                curso = Curso(
                        nome = "spring boot 1",
                        categoria = "Programcao"
                )
        )
        return TopicoDto.converter2(listOf(topico, topico, topico))
    }

    @GetMapping
    @Cacheable(value = ["listaDeTopicos"])
    fun lista(
            @RequestParam(required = false) nomeCurso: String?,
            @PageableDefault(sort = ["id"], direction = Sort.Direction.DESC, page = 0, size = 10) paginacao: Pageable
    ): Page<TopicoDto> {

        if (nomeCurso == null) {
            val topicos: Page<Topico> = topicoRepository.findAll(paginacao)
            return TopicoDto.converter(topicos)
        } else {
            val topicos: Page<Topico> = topicoRepository.findByCursoNome(nomeCurso, paginacao)
            return TopicoDto.converter(topicos)
        }
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = ["listaDeTopicos"], allEntries = true)
    fun cadastrar(
            @Valid @RequestBody request: TopicoRequest,
            uriBuilder: UriComponentsBuilder
    ): ResponseEntity<TopicoDto> {

        val topico: Topico = request.converter(cursoRepository)
        topicoRepository.save(topico)

        var uri: URI = uriBuilder.path("/topicos/{id}").buildAndExpand(topico.id).toUri()
        return ResponseEntity.created(uri).body(TopicoDto(topico))
    }

    @GetMapping("/{id}")
    fun detalhar(@PathVariable id: Long): ResponseEntity<DetalhesTopicoDto> {
        val topico: Optional<Topico> = topicoRepository.findById(id)
        if(topico.isPresent) {
            return ResponseEntity.ok(DetalhesTopicoDto(topico.get()))
        }
        return ResponseEntity.notFound().build()
    }

    @RequestMapping(value = ["/{id}"], method = arrayOf(RequestMethod.PUT))
    @Transactional
    @CacheEvict(value = ["listaDeTopicos"], allEntries = true)
    fun atualizar(
            @PathVariable id: Long,
            @Valid @RequestBody requestUpdate: TopicoUpdateRequest
    ): ResponseEntity<TopicoDto> {
        val optional: Optional<Topico> = topicoRepository.findById(id)
        if(optional.isPresent){
            val topico: Topico = requestUpdate.atualizar(id, topicoRepository)
            return ResponseEntity.ok(TopicoDto(topico))
        }
        return ResponseEntity.notFound().build()
    }

    @DeleteMapping("/{id}")
    @Transactional
    @CacheEvict(value = ["listaDeTopicos"], allEntries = true)
    fun deletar(@PathVariable id: Long): ResponseEntity<TopicoDto> {
        val optional: Optional<Topico> =  topicoRepository.findById(id)
        if (optional.isPresent) {
            topicoRepository.deleteById(id)
            return ResponseEntity.ok().build()
        }
        return ResponseEntity.notFound().build()
    }
}




