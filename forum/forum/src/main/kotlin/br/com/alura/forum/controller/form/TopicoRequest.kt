package br.com.alura.forum.controller.form

import br.com.alura.forum.modelo.Curso
import br.com.alura.forum.modelo.Topico
import br.com.alura.forum.repository.CursoRepository
import org.hibernate.validator.constraints.Length
import org.jetbrains.annotations.NotNull
import javax.validation.constraints.NotEmpty


class TopicoRequest(
//        @NotEmpty (message = "Please enter your email addresss")

        @field: NotNull
        @field: NotEmpty (message = "Por favor, informe o título")
        @field: Length(min = 5)
        var titulo: String,

        @field: NotNull
        @field: NotEmpty (message = "Por favor, informe a mensagem")
        @field: Length(min = 10)
        var mensagem: String,

        @field: NotNull
        @field: NotEmpty (message = "Por favor, informe o nome do curso")
        var nomeCurso: String
) {

    fun converter(cursoRepository: CursoRepository): Topico {
        val curso: Curso = cursoRepository.findByNome(nomeCurso)
        return Topico(titulo, mensagem, curso)
    }
}