package br.com.alura.forum.config.validacao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.http.HttpStatus
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.util.*

@RestControllerAdvice
class ErroValidacaoHandler(
        @Autowired
        val messageSource: MessageSource
) {

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun handle(exception: MethodArgumentNotValidException): List<ErroDeFormularioDto>{
        val dto = ArrayList<ErroDeFormularioDto>()
        val fieldErros: List<FieldError> = exception.getBindingResult().getFieldErrors()

        fieldErros.forEach { e->
            val mensagem: String = messageSource.getMessage(e, LocaleContextHolder.getLocale())
            val erro = ErroDeFormularioDto(e.field, mensagem)
            dto.add(erro)
        }

        return dto
    }
}