package br.com.alura.forum.modelo

import java.time.LocalDateTime
import javax.persistence.*

@Entity
class Topico(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="id")
        val id: Long = 0,

        var titulo: String = "",
        var mensagem: String = "",
        val dataCriacao: LocalDateTime = LocalDateTime.now(),

        @Enumerated(EnumType.STRING)
        val status: StatusTopico = StatusTopico.NAO_RESPONDIDO,

        @ManyToOne(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.ALL))
        @JoinColumn(name="autor_id", referencedColumnName = "autor_id")
        val autor: Usuario = Usuario(),

        @ManyToOne(fetch = FetchType.EAGER, cascade = arrayOf(CascadeType.ALL))
        @JoinColumn(name="curso_id", referencedColumnName = "curso_id")
        var curso: Curso = Curso(),

        @OneToMany(mappedBy = "topico",fetch = FetchType.EAGER, cascade = arrayOf(CascadeType.ALL))
        val respostas: List<Resposta> = ArrayList()
) {

    constructor(titulo: String, mensagem: String, curso: Curso): this() {
        this.titulo = titulo
        this.mensagem = mensagem
        this.curso = curso
    }


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Topico

        if (id != other.id) return false
        if (titulo != other.titulo) return false
        if (mensagem != other.mensagem) return false
        if (dataCriacao != other.dataCriacao) return false
        if (status != other.status) return false
        if (autor != other.autor) return false
        if (curso != other.curso) return false
        if (respostas != other.respostas) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + titulo.hashCode()
        result = 31 * result + mensagem.hashCode()
        result = 31 * result + (dataCriacao?.hashCode() ?: 0)
        result = 31 * result + status.hashCode()
        result = 31 * result + autor.hashCode()
        result = 31 * result + curso.hashCode()
        result = 31 * result + respostas.hashCode()
        return result
    }

}

