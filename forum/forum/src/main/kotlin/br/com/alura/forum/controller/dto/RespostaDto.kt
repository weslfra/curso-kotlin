package br.com.alura.forum.controller.dto

import br.com.alura.forum.modelo.Topico
import java.time.LocalDateTime

class RespostaDto(resposta: Topico) {

    var id: Long
    var mensagem: String
    var dataCriacao: LocalDateTime
    var nomeAutor: String

    init {
        this.id = resposta.id
        this.mensagem = resposta.mensagem
        this.dataCriacao = resposta.dataCriacao
        this.nomeAutor = resposta.autor.nome!!
    }

    companion object{
        fun converter (topicos: List<Topico>): List<RespostaDto>{
            return topicos.map { RespostaDto(it) }
        }
    }
}



